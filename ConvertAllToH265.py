import sys
import os
import subprocess

def main():
    FFMPEG_Binary = '/Volumes/RAID/Video_H265_Convert/ffmpeg'
    print "Starting"
    listOfFiles = []
    for f in fileListGen(inputDir):
        fExt = os.path.splitext(f)[1]
        # if fExt == ".mov":
        #     listOfFiles.append(f)
        listOfFiles.append(f)

    for sourceFile in listOfFiles:
        os.chdir(inputDir)
        #subprocess.call([FFMPEG_Binary, "-i", sourceFile, "-c:v", "libx265", "-preset", "medium", "-crf", "20", "-c:a", "aac", "-b:a", "128k", '{0}_h265.mp4'.format(sourceFile[:-4])], cwd=inputDir)
        subprocess.call([FFMPEG_Binary, "-i", sourceFile, "-threads", "0", "-c:v", "libx265", "-preset", "medium", "-crf", "20", "-c:a", "aac", "-b:a", "128k", '{0}_h265.mp4'.format(sourceFile)], cwd=inputDir)
        #TODO: Copy tags from source to destination file

def fileListGen(root_dir):
    for root, dirs, files in os.walk(root_dir):
        for name in files:
            path = os.path.join(root, name)
            if (os.path.islink(path)):
                pass
            else:
                yield path

if __name__ == "__main__":
    #Variables:
    inputDir = sys.argv[1]
    main()